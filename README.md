# The packaging information for python python-flask-cors in XiVO

This repository contains the packaging information for
[python-flask-cors](https://github.com/corydolphin/flask-cors).

To get a new version of python-flask-cors in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.

